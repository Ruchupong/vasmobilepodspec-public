Pod::Spec.new do |s|
    s.name            = "VasSmartCardReader"
    s.version         = "2.1.0"
    s.summary         = "Library SmartCardReader (FEITIAN_MOBILE_READERS)"
    s.description     = "Library SmartCardReader (FEITIAN_MOBILE_READERS) for iOS"
    s.homepage        = "https://gitlab.com/mobilevas/vasmobileframework-public"
    s.license         = { :type => 'MIT', :file => 'LICENSE' }
    s.author          = { "Ruchupong Saengan" => "Ruchupong_sae@truecorp.co.th" }
    s.source          = { :git => "git@gitlab.com:mobilevas/vasmobileframework-public.git", :tag => "vassmartcardreader-2.1.0" }
    s.platform        = :ios, "11.0"
    s.module_name     = s.name

    s.vendored_frameworks    = "VasSmartCardReader/Framework/*.xcframework"

  end
  
  